/**
 * 
 */
package fr.ldnr.projet.JCA;

import java.time.LocalDate;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Azryelh
 *
 */
@Controller
public class BiblioController implements ErrorController{
	private BookService bookService;
	private BorrowService borrowService;
	private static final Logger logger = LoggerFactory.getLogger(Main.class);
	
	@RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        // get error status
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        // TODO: log error details here

        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());

            // display specific error page
            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return "error";
            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                return "error";
            } else if (statusCode == HttpStatus.FORBIDDEN.value()) {
                return "error";
            }
        }

        // display generic error
        return "error";
    }

	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String displayHomeBook() {
		return "NewBook";
	}

	@RequestMapping(path = "/book", method = RequestMethod.GET)
	public String displayBook() {
		return "NewBook";
	}

	@RequestMapping(path = "/borrow", method = RequestMethod.GET)
	public String displayBorrow(Model model) {
		model.addAttribute("NewBook", bookService.readAllBook());
		model.addAttribute("BorrowList", borrowService.readAllBorrow());
		return "BorrowList";
	}

	@PostMapping("/book")
	public String receptBook(Model modelBook, @RequestParam("title") String title,
			@RequestParam("editor") String editor, @RequestParam("editionYear") String year,
			@RequestParam("autorFirstName") String autorFirstName,
			@RequestParam("autorLastName") String autorLastName) {
		return receptBookGeneral(modelBook, title, editor, year, autorFirstName, autorLastName);
	}

	@PostMapping("/")
	public String receptBook2(Model modelBook, @RequestParam("title") String title,
			@RequestParam("editor") String editor, @RequestParam("editionYear") String year,
			@RequestParam("autorFirstName") String autorFirstName,
			@RequestParam("autorLastName") String autorLastName) {
		return receptBookGeneral(modelBook, title, editor, year, autorFirstName, autorLastName);
	}

	private String receptBookGeneral(Model modelBook, @RequestParam("title") String title,
			@RequestParam("editor") String editor, @RequestParam("editionYear") String year,
			@RequestParam("autorFirstName") String autorFirstName,
			@RequestParam("autorLastName") String autorLastName) {
		logger.info("Enter postMap book");
		if (title.length() < 3 || editor.length() < 3 || year.length() < 3 || autorFirstName.length() < 3
				|| autorLastName.length() < 3) {
			logger.info("enter error book");
			modelBook.addAttribute("message", "Erreurs dans le formulaire");
			return "NewBook";
		} else {
			logger.info("Enter ok book");
			bookService.save(title, editor, year, autorFirstName, autorLastName, false);
			modelBook.addAttribute("message", "Livre bien ajouté");
			return "NewBook";
		}
	}

	@PostMapping("/borrow")
	public String receptBorrow(Model model, @RequestParam("drone") String radioValue, @RequestParam("book") String id,
			@RequestParam("borrower") String borrowerLastName, @RequestParam("borrowedDate") String dateBorrow,
			@RequestParam("lost") String perdu) {
		// log a sup
		logger.info("Perdu value : " + perdu);
		if (radioValue.equals("createBorrow")) {
			logger.info("Enter borrow postmapping");
			if (!bookService.bookExist(Integer.parseInt(id)) || bookService.bookBorrow(Integer.parseInt(id))
					|| borrowerLastName.length() < 3) {
				logger.info("Enter Borrow fail");
				model.addAttribute("message", "Erreurs dans le formulaire d'ajout");
				model.addAttribute("NewBook", bookService.readAllBook());
				model.addAttribute("BorrowList", borrowService.readAllBorrow());
				return "BorrowList";
			} else {
				logger.info("Enter borrow ok");
				LocalDate date1 = LocalDate.parse(dateBorrow);
				bookService.modify(Integer.parseInt(id), true);
				borrowService.save(Integer.parseInt(id), borrowerLastName, date1);
				model.addAttribute("message", "Ajout d'emprunt bien sauvegarder");
				model.addAttribute("NewBook", bookService.readAllBook());
				model.addAttribute("BorrowList", borrowService.readAllBorrow());
				return "BorrowList";
			}
		} else if (radioValue.equals("modifyBorrow")) {
			logger.info("Enter borrow postmapping");
			if (!borrowService.borrowExist(Integer.parseInt(id))) {
				logger.info("Enter Borrow fail");
				model.addAttribute("message", "Erreurs dans le formulaire de modification");
				model.addAttribute("NewBook", bookService.readAllBook());
				model.addAttribute("BorrowList", borrowService.readAllBorrow());
				return "BorrowList";
			} else {
				logger.info("Enter borrow ok");
				LocalDate date1 = LocalDate.parse(dateBorrow);
				int bookId = borrowService.modify(Integer.parseInt(id), date1);
				logger.info("Enter borrow modify borrow");
				if (perdu.equals("rendreLivre")) {
					bookService.modify(bookId, false);
				}
				logger.info("Enter borrow modify book");
				model.addAttribute("message", "Cloture emprunt bien update");
				model.addAttribute("NewBook", bookService.readAllBook());
				model.addAttribute("BorrowList", borrowService.readAllBorrow());
				return "BorrowList";
			}

		} else {
			logger.info("No radio Tcheck");
			model.addAttribute("message", "Aucun choix séléctionner");
			model.addAttribute("NewBook", bookService.readAllBook());
			model.addAttribute("BorrowList", borrowService.readAllBorrow());
			return "BorrowList";
		}
	}

	@Autowired
	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	@Autowired
	public void setBorrowService(BorrowService borrowService) {
		this.borrowService = borrowService;
	}

}
