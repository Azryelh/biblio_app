package fr.ldnr.projet.JCA;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "books")
public class Book {
	private int id;
	private String title;
	private String editor;
	private String year;
	private String autorFirstName;
	private String autorLastName;
	Boolean isBorrow;

	public Book() {
		this(null, null, null, null, null, false);
	}

	public Book(String title, String editor, String year, String autorFirstName, String autorLastName,
			Boolean isBorrow) {
		super();
		this.id = 0;
		this.title = title;
		this.editor = editor;
		this.year = year;
		this.autorFirstName = autorFirstName;
		this.autorLastName = autorLastName;
		this.isBorrow = isBorrow;
	}

	@Id // indexé et unique
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(length = 60, nullable = false)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(length = 60, nullable = false)
	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	@Column(length = 5, nullable = false)
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Column(length = 60, nullable = false)
	public String getAutorFirstName() {
		return autorFirstName;
	}

	public void setAutorFirstName(String autorFirstName) {
		this.autorFirstName = autorFirstName;
	}

	@Column(length = 60, nullable = false)
	public String getAutorLastName() {
		return autorLastName;
	}

	public void setAutorLastName(String autorLastName) {
		this.autorLastName = autorLastName;
	}

	@Column(nullable = false)
	public Boolean getIsBorrow() {
		return isBorrow;
	}

	public void setIsBorrow(Boolean isBorrow) {
		this.isBorrow = isBorrow;
	}
}
