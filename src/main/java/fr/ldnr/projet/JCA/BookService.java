package fr.ldnr.projet.JCA;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {
	private SessionFactory sessionFactory;
	private static final Logger logger = LoggerFactory.getLogger(Main.class);

	@Autowired
	public void setSf(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void save(String title, String editor, String year, String autorFirstName, String autorLastName, Boolean isBorrow) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(new Book(title, editor, year, autorFirstName, autorLastName, false));
		tx.commit();
		session.close();
	}

	public List<Book> readAllBook() {
		Session session = sessionFactory.openSession();
		List<Book> listeDeamnde = session.createQuery("from Book", Book.class).list();
		session.close();
		return listeDeamnde;
	}

	public void modify(int id, Boolean isBorrow) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Book bookModify = session.load(Book.class, id);
		bookModify.setIsBorrow(isBorrow);
		session.update(bookModify);
		tx.commit();
		session.close();
	}
	
	public boolean bookExist(int id) {
		Session session = sessionFactory.openSession();
		try {
			logger.info("Enter bookExist");
			Book bookModify = session.load(Book.class, id);
			logger.info("book :"+bookModify.getId()+" "+bookModify.getTitle());			
			logger.info("BookExist OK");
			session.close();
			return true;
		}catch(Exception e) {
			logger.error("Fail bookExist");
			session.close();
			return false;
		}		
	}
	
	public boolean bookBorrow(int id) {	
		Session session = sessionFactory.openSession();
		Book bookBorrow = session.load(Book.class, id);	
		logger.info("Enter bookBorrow id boolk : " +id);
		logger.info("bookBorrow : isBorow ? : "+bookBorrow.getIsBorrow());
		session.close();
		return bookBorrow.getIsBorrow();
	}
}
