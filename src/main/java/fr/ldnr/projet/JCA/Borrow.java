package fr.ldnr.projet.JCA;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Azryelh
 *
 */
@Entity
public class Borrow {

	private int id;
	private int bookId;
	private String borrowerLastName;
	private LocalDate dateBorrow;
	private Boolean rendering;
	

	public Borrow() {
		this(0, null, null,false);
	}

	public Borrow(int bookId, String borrowerLastName, LocalDate dateBorrow , Boolean rendering) {
		super();
		this.bookId = bookId;
		this.borrowerLastName = borrowerLastName;
		this.dateBorrow = dateBorrow;
		this.rendering = rendering;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	public int getBook() {
		return bookId;
	}

	public void setBook(int bookId) {
		this.bookId = bookId;
	}

	@Column(nullable = false, length = 100)
	public String getBorrowerLastName() {
		return borrowerLastName;
	}

	public void setBorrowerLastName(String borrowerLastName) {
		this.borrowerLastName = borrowerLastName;
	}

	public LocalDate getDateBorrow() {
		return dateBorrow;
	}

	public void setDateBorrow(LocalDate dateBorrow) {
		this.dateBorrow = dateBorrow;
	}

	@Override
	public String toString() {
		return "Borrow [id=" + id + ", borrowerLastName=" + borrowerLastName + ", dateBorrow=" + dateBorrow + "]";
	}

	@Column
	public Boolean getRendering() {
		return rendering;
	}

	public void setRendering(Boolean rendering) {
		this.rendering = rendering;
	}
	
	

}
