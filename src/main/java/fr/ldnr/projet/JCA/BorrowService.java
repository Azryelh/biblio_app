/**
 * 
 */
package fr.ldnr.projet.JCA;

import java.time.LocalDate;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Azryelh
 *
 */
@Service
public class BorrowService {
	private SessionFactory sessionFactory;
	private static final Logger logger = LoggerFactory.getLogger(Main.class);

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void save(int bookId, String borrowerLastName, LocalDate dateBorrow) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(new Borrow(bookId, borrowerLastName, dateBorrow,false));
		tx.commit();
		session.close();
	}

	public List<Borrow> readAllBorrow() {
		Session session = sessionFactory.openSession();
		List<Borrow> borrowList = session.createQuery("from Borrow order by dateBorrow", Borrow.class).list();
		session.close();
		return borrowList;
	}

	public int modify(int id, LocalDate date) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Borrow borrowMod = session.load(Borrow.class, id);
		borrowMod.setDateBorrow(date);
		borrowMod.setRendering(true);
		session.update(borrowMod);
		tx.commit();
		session.close();
		return borrowMod.getBook();
	}
	
	public boolean borrowExist(int id) {
		Session session = sessionFactory.openSession();
		try {
			logger.info("Enter borrowExist");
			Borrow borrowMod = session.load(Borrow.class, id);
			logger.info("borrow :"+borrowMod.getBook());		
			logger.info("BorrowExist OK");
			session.close();
			return true;
		}catch(Exception e) {
			logger.error("Fail borrowExist");
			session.close();
			return false;
		}		
	}

}
