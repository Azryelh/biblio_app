/**
 * 
 */
package fr.ldnr.projet.JCA;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * @author Azryelh
 *
 */
@SpringBootApplication
public class Main {

	private static final Logger logger = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
		logger.info("App. BiblioApp started");
		logger.info("http://localhost:8081/");
	}

	@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver vr = new InternalResourceViewResolver();
		vr.setViewClass(JstlView.class);
		vr.setPrefix("/WEB-INF/vues/");
		vr.setSuffix(".jsp");
		return vr;
	}

	@Bean
	public SessionFactory getSessionFactory() {
		Properties options = new Properties();
		options.put("hibernate.dialect", "org.sqlite.hibernate.dialect.SQLiteDialect");
		options.put("hibernate.connection.driver_class", "org.sqlite.JDBC");
		options.put("hibernate.connection.url", "jdbc:sqlite:BiblioApp.sqlite");
		options.put("hibernate.hbm2ddl.auto", "update");
		options.put("hibernate.show_sql", "true");
		SessionFactory sf = new Configuration()
				.addProperties(options)
				.addAnnotatedClass(Borrow.class)
				.addAnnotatedClass(Book.class)
				.buildSessionFactory(); 
		return sf;
	}
}
