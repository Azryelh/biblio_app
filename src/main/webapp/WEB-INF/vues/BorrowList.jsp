<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="fr">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<title>Emprunt</title>
<script src="./js/scriptBorrowForm.js"></script>
</head>

<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a href="/" className="navbar-left"> <img
				src="./img/logo-ldnr.jpg" alt="logo-ldnr" />
			</a> <a class="navbar-brand" href="#">Biblio App</a>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link" href="./book">
							Livres<span class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="./borrow">Emprunt
							de livres</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<br />
	<p>&nbsp;&nbsp;Gestion d'emprunt</p>

	<div class="container">
		<table class="table table-striped">
			<thead>
				<tr>
					<th colspan="9" class="text-center">Liste des livres</th>
					<!--La page des emprunts montre en plus la liste des emprunts en cours,
            dans l'ordre chronologique d'emprunt.-->
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>Id</th>
					<th>Livre</th>
					<th>Ann�e d'�dition</th>
					<th>Editeur</th>
					<th>Auteur Nom</th>
					<th>Auteur Pr�nom</th>
					<th>Disponible</th>

				</tr>
				<c:forEach items="${NewBook}" var="book">
					<tr>
						<td>${book.id}</td>
						<td>${book.title}</td>
						<td>${book.year}</td>
						<td>${book.editor}</td>
						<td>${book.autorLastName}</td>
						<td>${book.autorFirstName}</td>
						<td>${book.isBorrow ? "non":"oui"}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<form method="POST">
			<div>
				<input type="radio" id="createBorrow" name="drone" value="createBorrow" checked>
				<label for="createBorrow">Cr�er d'un emprunt&nbsp;&nbsp;</label> 
				<input type="radio" id="modifyBorrow" name="drone" value="modifyBorrow">
				<label for="modifyBorrow">Cl�turer l'emprunt</label>
			</div>			
			<div class="form-group">
				<label for="book" id="textId"></label> <input
					type="text" class="form-control" id="book" name="book"
					placeholder="Entrez Id">
			</div>
			<div class="form-group" id="borrowerDiv">
				<label for="borrower">Emprunteur</label> <input type="text"
					class="form-control" id="borrower" name="borrower"
					placeholder="Entrez le nom de l'emprunteur">
			</div>
			<div class="form-group">
				<label for="borrowedDate" id="textDate"></label> <input type="date" id="dateBorrow"
					class="form-control" name="borrowedDate">
				<!-- placeholder="Entrez la date d'emprunt" -->
			</div>
			<div id="lostBookDiv">
				<input type="radio" id="nolost" name="lost"	value=rendreLivre checked>
				<label for="nolost">Rendre le livre</label> 
				<input type="radio" id="booklost" name="lost" value="perduLivre">
				<label for="booklost">Livre perdu</label>
			</div>
			<button type="submit" class="btn btn-primary" id="submitBorrow">Submit</button>
			<!--add a rental book-->
			<p>${message}</p>
		</form>
		<table class="table table-striped">
			<thead>
				<tr>
					<th colspan="5" class="text-center">Liste des emprunts en
						cours</th>
					<!--La page des emprunts montre en plus la liste des emprunts en cours,
          dans l'ordre chronologique d'emprunt.-->
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>Id</th>
					<th>Livre</th>
					<th>Emprunteur</th>
					<!--borrower's last name-->
					<th>Date de rendu</th>
					<th>Cl�tur�</th>
				</tr>
				<c:forEach items="${BorrowList}" var="borrow">
					<tr>
						<td>${borrow.id}</td>
						<td>${borrow.book}</td>
						<td>${borrow.borrowerLastName}</td>
						<td>${borrow.dateBorrow}</td>
						<td>${borrow.rendering ? "oui":"non"}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<!--<button type="submit" class="btn btn-primary">Modifier un emprunt</button>-->
	</div>
	<footer>
		<p colspan="5" class="text-center">LDNR � 2022 | Designed by Group
			1 ( Colin, Gabriel, Jiyeong )</p>
		<!--in alphabetical order-->
	</footer>
</body>

</html>