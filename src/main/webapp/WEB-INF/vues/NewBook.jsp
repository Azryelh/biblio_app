<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- w3c bootstrap get started -->
  <!-- Latest compiled and minified CSS (Bootstrap)-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <title>Livre</title>
  <script src="./js/scriptBookForm.js"></script>
  </head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a href="/" className="navbar-left"><!--logo-->
        <img src="./img/logo-ldnr.jpg" alt="logo-ldnr"/>
      </a>
      <a class="navbar-brand">Biblio App</a>
      <!--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button> -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="./book">Livres <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./borrow">Emprunt de livres</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <br/>
  <p>&nbsp;&nbsp;Cr�ation de livre</p>
  <div class="container">
    <form method="POST">
      <div class="form-group">
        <label for="title">Titre</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Entrez le titre du livre">
      </div>
      <div class="form-group">
        <label for="editionYear">Ann�e d'�dition</label>
        <input type="text" class="form-control" id="editionYear" name="editionYear" placeholder="Entrez l'ann�e d'�dition">
      </div>
      <div class="form-group">
        <label for="editor">Editeur</label>
        <input type="text" class="form-control" id="editor" name="editor" placeholder="Entrez l'�diteur">
      </div>
      <div class="form-group">
        <label for="autorFirstName">Auteur Nom</label>
        <input type="text" class="form-control" id="autorFirstName" name="autorFirstName" placeholder="Entrez le nom de l'auteur">
      </div>
      <div class="form-group">
        <label for="autorLastName">Auteur Pr�nom</label>
        <input type="text" class="form-control" id="autorLastName" name="autorLastName" placeholder="Entrez le pr�nom de l'auteur">
      </div>
      <button type="submit" class="btn btn-primary" id="submitBook">Cr�er un livre</button>
      <p>${message}</p>
    </form><br/>
  </div><br/>
  <!-- <div class="footer-dark"> -->
  <footer>
    <p  colspan="5" class="text-center">LDNR � 2022 | Designed by Group 1 ( Colin, Gabriel, Jiyeong )</p>
    <!--in alphabetical order-->
  </footer>
  <!--</div>-->   
</body>

</html>