<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- w3c bootstrap get started -->
    <!-- Latest compiled and minified CSS (Bootstrap)-->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <title>Page d'erreur</title>
</head>

<body>
    <div class="d-flex align-items-center justify-content-center vh-100">
        <div class="text-center">
            <!--<h1 class="display-1 fw-bold">404</h1>-->
            <p class="fs-3"> <span class="text-danger">Opps !</span> Page non trouvée.</p>
            <p class="lead">Une erreur est survenue. Cette page n'existe pas.</p>
            <a href="book" class="btn btn-primary">Home</a>
            <!--to do : modify href="book"-->
        </div>
    </div>
</body>

</html>