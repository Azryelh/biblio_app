document.addEventListener("DOMContentLoaded", evt => {
	const affichage = setInterval(verifFlag, 500);
	function verifFlag() {
		let flag = true;
		const title = document.getElementById("title");
		const editionYear = document.getElementById("editionYear");
		const editor = document.getElementById("editor");
		const autorFirstName = document.getElementById("autorFirstName");
		const autorLastName = document.getElementById("autorLastName");
		if (title.value.trim().length == 0) {
			flag = false;
		}
		if (editionYear.value.trim().length != 4) {
			flag = false;
		}
		if (editor.value.trim().length == 0) {
			flag = false;
		}
		if (autorFirstName.value.trim().length == 0) {
			flag = false;
		}
		if (autorLastName.value.trim().length == 0) {
			flag = false;
		}
		

		const submit = document.getElementById("submitBook")
		if (!flag) {
			submit.disabled = true;
		} else {
			submit.disabled = false;
		}
	}
})