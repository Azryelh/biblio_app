document.addEventListener("DOMContentLoaded", evt => {
	const affichage = setInterval(verifFlag, 500);
	function verifFlag() {
		let flag = true;
		const newBorrowRB = document.getElementById("createBorrow");
		const idBook = document.getElementById("book");
		if (idBook.value.trim().length == 0) {
			flag = false;
		}
		const borrowerDiv = document.getElementById("borrowerDiv");
		const borrower = document.getElementById("borrower");
		const divLost = document.getElementById("lostBookDiv");
		const txtId = document.getElementById("textId");
		const txtDate = document.getElementById("textDate");
		//choix du type de form
		if (newBorrowRB.checked) {
			/*borrowerDiv.style.visibility="visible";*/
			borrowerDiv.style.display = "block";
			if (borrower.value.trim().length == 0) {
				flag = false;
			}
			divLost.style.display = "none";
			txtId.textContent = "Id du livre";
			txtDate.textContent = "Date d'emprunt";
		} else {
			borrowerDiv.style.display = "none";
			divLost.style.display = "block";
			txtId.textContent = "Id de l'emprunt";
			txtDate.textContent = "Date de clôture";
		}

		const date = document.getElementById("dateBorrow");
		// Trim de l'element HTML pour verifier if not null
		if(date.value.trim().length < 1){
			flag = false;
		}

		const submit = document.getElementById("submitBorrow")
		if (!flag) {
			submit.disabled = true;
		} else {
			submit.disabled = false;
		}
	}
})
		
		